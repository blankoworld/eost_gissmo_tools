# Gissmo Launch Script

This script, called **Gissmo Launcher**, aims to manage Docker containers while working on Gissmo project (Cf. https://github.com/eost/gissmo/).

It's not a part of previous Github's branch because it's only for Docker use.

## Requirements

I suggest you to check these points befaure using Gissmo Launcher:

  * docker
  * bash (Bourne Again Shell)
  * tput command (try ```which tput``` to know if you have it)
  * a default configuration file to give Gissmo Launcher some data

**Note**: This script was done on Ubuntu. If you use another operating system, some commands will not work. For an example `launch.sh init`.

## Configuration file

A configuration example is available here: [config.example](./config.example).

Gissmo Launch Script search a file called **config** located in XDG_CONFIG_HOME.

If not, it searches this file: **.config/gissmo** located in your home directory.

## Usage

Just do:

    ./launch.sh

It will display you a list of available commands.

## Examples

Here is an example on how I frequently use Gissmo Launcher:

    launch.sh init
    launch.sh create
    launch.sh restore ~/2015.05.06.dump
    launch.sh migrate
    launch.sh dev
    launch.sh stop
    launch.sh start python manage.py shell
    launch.sh stop

## A different configuration file

If you want to use another configuration file than current, use the **GISSMO_CFG** variable as:

    GISSMO_CFG=~/new_config launch.sh

It will use *new_config* file instead.

## License

This tool is under [WTFPL – Do What the Fuck You Want to Public License](http://www.wtfpl.net/).
